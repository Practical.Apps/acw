﻿namespace Awesome_Certificate_Wizard
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.Name_File_Button = new System.Windows.Forms.Button();
            this.Name_File_Textbox = new System.Windows.Forms.TextBox();
            this.Name_File_Label = new System.Windows.Forms.Label();
            this.Presentation_File_Label = new System.Windows.Forms.Label();
            this.Presentation_File_Textbox = new System.Windows.Forms.TextBox();
            this.Presentation_File_Button = new System.Windows.Forms.Button();
            this.Parameter_File_Label = new System.Windows.Forms.Label();
            this.Parameter_File_Textbox = new System.Windows.Forms.TextBox();
            this.Parameter_File_Button = new System.Windows.Forms.Button();
            this.Save_parameters_button = new System.Windows.Forms.Button();
            this.Create_presentation_button = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Destination_Folder_Button = new System.Windows.Forms.Button();
            this.Destination_textbox = new System.Windows.Forms.TextBox();
            this.Name_placement_label = new System.Windows.Forms.Label();
            this.Location_X_Textbox = new System.Windows.Forms.TextBox();
            this.Location_Y_Textbox = new System.Windows.Forms.TextBox();
            this.X_Label = new System.Windows.Forms.Label();
            this.Y_Label = new System.Windows.Forms.Label();
            this.Certificate_Name_Textbox = new System.Windows.Forms.TextBox();
            this.Certificate_Series_Label = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Name_File_Button
            // 
            this.Name_File_Button.Location = new System.Drawing.Point(96, 38);
            this.Name_File_Button.Name = "Name_File_Button";
            this.Name_File_Button.Size = new System.Drawing.Size(30, 25);
            this.Name_File_Button.TabIndex = 0;
            this.Name_File_Button.Text = "@";
            this.Name_File_Button.UseVisualStyleBackColor = true;
            this.Name_File_Button.Click += new System.EventHandler(this.open_name_file);
            // 
            // Name_File_Textbox
            // 
            this.Name_File_Textbox.Location = new System.Drawing.Point(132, 41);
            this.Name_File_Textbox.Name = "Name_File_Textbox";
            this.Name_File_Textbox.Size = new System.Drawing.Size(237, 20);
            this.Name_File_Textbox.TabIndex = 1;
            // 
            // Name_File_Label
            // 
            this.Name_File_Label.AutoSize = true;
            this.Name_File_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name_File_Label.Location = new System.Drawing.Point(5, 41);
            this.Name_File_Label.Name = "Name_File_Label";
            this.Name_File_Label.Size = new System.Drawing.Size(85, 15);
            this.Name_File_Label.TabIndex = 2;
            this.Name_File_Label.Text = "List of Names:";
            // 
            // Presentation_File_Label
            // 
            this.Presentation_File_Label.AutoSize = true;
            this.Presentation_File_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Presentation_File_Label.Location = new System.Drawing.Point(5, 103);
            this.Presentation_File_Label.Name = "Presentation_File_Label";
            this.Presentation_File_Label.Size = new System.Drawing.Size(76, 15);
            this.Presentation_File_Label.TabIndex = 5;
            this.Presentation_File_Label.Text = "PwrPnT File:";
            // 
            // Presentation_File_Textbox
            // 
            this.Presentation_File_Textbox.Location = new System.Drawing.Point(132, 102);
            this.Presentation_File_Textbox.Name = "Presentation_File_Textbox";
            this.Presentation_File_Textbox.Size = new System.Drawing.Size(237, 20);
            this.Presentation_File_Textbox.TabIndex = 4;
            // 
            // Presentation_File_Button
            // 
            this.Presentation_File_Button.Location = new System.Drawing.Point(96, 99);
            this.Presentation_File_Button.Name = "Presentation_File_Button";
            this.Presentation_File_Button.Size = new System.Drawing.Size(30, 25);
            this.Presentation_File_Button.TabIndex = 3;
            this.Presentation_File_Button.Text = "@";
            this.Presentation_File_Button.UseVisualStyleBackColor = true;
            this.Presentation_File_Button.Click += new System.EventHandler(this.open_presentation_file);
            // 
            // Parameter_File_Label
            // 
            this.Parameter_File_Label.AutoSize = true;
            this.Parameter_File_Label.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Parameter_File_Label.Location = new System.Drawing.Point(5, 72);
            this.Parameter_File_Label.Name = "Parameter_File_Label";
            this.Parameter_File_Label.Size = new System.Drawing.Size(91, 15);
            this.Parameter_File_Label.TabIndex = 8;
            this.Parameter_File_Label.Text = "Parameter File:";
            // 
            // Parameter_File_Textbox
            // 
            this.Parameter_File_Textbox.Location = new System.Drawing.Point(132, 72);
            this.Parameter_File_Textbox.Name = "Parameter_File_Textbox";
            this.Parameter_File_Textbox.Size = new System.Drawing.Size(237, 20);
            this.Parameter_File_Textbox.TabIndex = 7;
            // 
            // Parameter_File_Button
            // 
            this.Parameter_File_Button.Location = new System.Drawing.Point(96, 69);
            this.Parameter_File_Button.Name = "Parameter_File_Button";
            this.Parameter_File_Button.Size = new System.Drawing.Size(30, 25);
            this.Parameter_File_Button.TabIndex = 6;
            this.Parameter_File_Button.Text = "@";
            this.Parameter_File_Button.UseVisualStyleBackColor = true;
            this.Parameter_File_Button.Click += new System.EventHandler(this.open_parameter_file);
            // 
            // Save_parameters_button
            // 
            this.Save_parameters_button.Location = new System.Drawing.Point(8, 184);
            this.Save_parameters_button.Name = "Save_parameters_button";
            this.Save_parameters_button.Size = new System.Drawing.Size(360, 26);
            this.Save_parameters_button.TabIndex = 14;
            this.Save_parameters_button.Text = "Save current settings";
            this.Save_parameters_button.UseVisualStyleBackColor = true;
            this.Save_parameters_button.Click += new System.EventHandler(this.save_to_parameter);
            // 
            // Create_presentation_button
            // 
            this.Create_presentation_button.Location = new System.Drawing.Point(8, 217);
            this.Create_presentation_button.Name = "Create_presentation_button";
            this.Create_presentation_button.Size = new System.Drawing.Size(361, 39);
            this.Create_presentation_button.TabIndex = 15;
            this.Create_presentation_button.Text = "CREATE AWESOME CERTIFICATES";
            this.Create_presentation_button.UseVisualStyleBackColor = true;
            this.Create_presentation_button.Click += new System.EventHandler(this.create_certificates);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(5, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 15);
            this.label1.TabIndex = 18;
            this.label1.Text = "Dstntn. Folder:";
            // 
            // Destination_Folder_Button
            // 
            this.Destination_Folder_Button.Location = new System.Drawing.Point(96, 8);
            this.Destination_Folder_Button.Name = "Destination_Folder_Button";
            this.Destination_Folder_Button.Size = new System.Drawing.Size(30, 25);
            this.Destination_Folder_Button.TabIndex = 16;
            this.Destination_Folder_Button.Text = "@";
            this.Destination_Folder_Button.UseVisualStyleBackColor = true;
            this.Destination_Folder_Button.Click += new System.EventHandler(this.open_dest_folder);
            // 
            // Destination_textbox
            // 
            this.Destination_textbox.Location = new System.Drawing.Point(132, 11);
            this.Destination_textbox.Name = "Destination_textbox";
            this.Destination_textbox.Size = new System.Drawing.Size(236, 20);
            this.Destination_textbox.TabIndex = 19;
            // 
            // Name_placement_label
            // 
            this.Name_placement_label.AutoSize = true;
            this.Name_placement_label.Location = new System.Drawing.Point(10, 129);
            this.Name_placement_label.Name = "Name_placement_label";
            this.Name_placement_label.Size = new System.Drawing.Size(229, 13);
            this.Name_placement_label.TabIndex = 20;
            this.Name_placement_label.Text = "Set name location (leave blank to used saved):";
            // 
            // Location_X_Textbox
            // 
            this.Location_X_Textbox.Location = new System.Drawing.Point(245, 126);
            this.Location_X_Textbox.Name = "Location_X_Textbox";
            this.Location_X_Textbox.Size = new System.Drawing.Size(28, 20);
            this.Location_X_Textbox.TabIndex = 21;
            this.Location_X_Textbox.TextChanged += new System.EventHandler(this.X_Text_Changed);
            // 
            // Location_Y_Textbox
            // 
            this.Location_Y_Textbox.Location = new System.Drawing.Point(307, 126);
            this.Location_Y_Textbox.Name = "Location_Y_Textbox";
            this.Location_Y_Textbox.Size = new System.Drawing.Size(28, 20);
            this.Location_Y_Textbox.TabIndex = 22;
            this.Location_Y_Textbox.TextChanged += new System.EventHandler(this.Y_Text_Changed);
            // 
            // X_Label
            // 
            this.X_Label.AutoSize = true;
            this.X_Label.Location = new System.Drawing.Point(282, 130);
            this.X_Label.Name = "X_Label";
            this.X_Label.Size = new System.Drawing.Size(14, 13);
            this.X_Label.TabIndex = 23;
            this.X_Label.Text = "X";
            // 
            // Y_Label
            // 
            this.Y_Label.AutoSize = true;
            this.Y_Label.Location = new System.Drawing.Point(347, 129);
            this.Y_Label.Name = "Y_Label";
            this.Y_Label.Size = new System.Drawing.Size(14, 13);
            this.Y_Label.TabIndex = 24;
            this.Y_Label.Text = "Y";
            // 
            // Certificate_Name_Textbox
            // 
            this.Certificate_Name_Textbox.Location = new System.Drawing.Point(132, 152);
            this.Certificate_Name_Textbox.Name = "Certificate_Name_Textbox";
            this.Certificate_Name_Textbox.Size = new System.Drawing.Size(236, 20);
            this.Certificate_Name_Textbox.TabIndex = 25;
            this.Certificate_Name_Textbox.TextChanged += new System.EventHandler(this.Certificate_Name_Change);
            // 
            // Certificate_Series_Label
            // 
            this.Certificate_Series_Label.AutoSize = true;
            this.Certificate_Series_Label.Location = new System.Drawing.Point(6, 155);
            this.Certificate_Series_Label.Name = "Certificate_Series_Label";
            this.Certificate_Series_Label.Size = new System.Drawing.Size(120, 13);
            this.Certificate_Series_Label.TabIndex = 26;
            this.Certificate_Series_Label.Text = "Certificate Series Name:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 266);
            this.Controls.Add(this.Certificate_Series_Label);
            this.Controls.Add(this.Certificate_Name_Textbox);
            this.Controls.Add(this.Y_Label);
            this.Controls.Add(this.X_Label);
            this.Controls.Add(this.Location_Y_Textbox);
            this.Controls.Add(this.Location_X_Textbox);
            this.Controls.Add(this.Name_placement_label);
            this.Controls.Add(this.Destination_textbox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Destination_Folder_Button);
            this.Controls.Add(this.Create_presentation_button);
            this.Controls.Add(this.Save_parameters_button);
            this.Controls.Add(this.Parameter_File_Label);
            this.Controls.Add(this.Parameter_File_Textbox);
            this.Controls.Add(this.Parameter_File_Button);
            this.Controls.Add(this.Presentation_File_Label);
            this.Controls.Add(this.Presentation_File_Textbox);
            this.Controls.Add(this.Presentation_File_Button);
            this.Controls.Add(this.Name_File_Label);
            this.Controls.Add(this.Name_File_Textbox);
            this.Controls.Add(this.Name_File_Button);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Awesome Certificate Wizard";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Name_File_Button;
        private System.Windows.Forms.TextBox Name_File_Textbox;
        private System.Windows.Forms.Label Name_File_Label;
        private System.Windows.Forms.Label Presentation_File_Label;
        private System.Windows.Forms.TextBox Presentation_File_Textbox;
        private System.Windows.Forms.Button Presentation_File_Button;
        private System.Windows.Forms.Label Parameter_File_Label;
        private System.Windows.Forms.TextBox Parameter_File_Textbox;
        private System.Windows.Forms.Button Parameter_File_Button;
        private System.Windows.Forms.Button Save_parameters_button;
        private System.Windows.Forms.Button Create_presentation_button;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Destination_Folder_Button;
        private System.Windows.Forms.TextBox Destination_textbox;
        private System.Windows.Forms.Label Name_placement_label;
        private System.Windows.Forms.TextBox Location_X_Textbox;
        private System.Windows.Forms.TextBox Location_Y_Textbox;
        private System.Windows.Forms.Label X_Label;
        private System.Windows.Forms.Label Y_Label;
        private System.Windows.Forms.TextBox Certificate_Name_Textbox;
        private System.Windows.Forms.Label Certificate_Series_Label;
    }
}

