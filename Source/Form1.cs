﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Excel = Microsoft.Office.Interop.Excel;
using Powerpoint = Microsoft.Office.Interop.PowerPoint;

namespace Awesome_Certificate_Wizard
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public string namefile_filename;
        public string presentationfile_filename;
        public string parameterfile_filename;
        public string destinationfolder_filepath;
        public string[] entryarray = new string[] { null, null, null, null, null, null, null };
        public bool[] selectionarray = new bool[] { false, false, false, false, false, false, false };


        private void open_name_file(object sender, EventArgs e)
        {
            OpenFileDialog open_namefile_dialog = new OpenFileDialog();
            open_namefile_dialog.Filter = "Excel Files (.xls)|*.xls|New Excel Files (.xlsx)|*.xlsx|All Files (*.*)|*.*";
            open_namefile_dialog.FilterIndex = 1;
            if (open_namefile_dialog.ShowDialog() == DialogResult.OK)
            {
                namefile_filename = open_namefile_dialog.FileName;
                selectionarray[0] = true;
            }
            Name_File_Textbox.Text = namefile_filename;
        }

        private void open_presentation_file(object sender, EventArgs e)
        {
            OpenFileDialog open_presfile_dialog = new OpenFileDialog();
            open_presfile_dialog.Filter = "Powerpoint Files (.ppt)|*.ppt|New Powerpoint Files (.pptx)|*.pptx|All Files (*.*)|*.*";
            open_presfile_dialog.FilterIndex = 1;
            if (open_presfile_dialog.ShowDialog() == DialogResult.OK)
            {
                presentationfile_filename = open_presfile_dialog.FileName;
                selectionarray[1] = true;
            }
            Presentation_File_Textbox.Text = presentationfile_filename;
        }

        private void open_parameter_file(object sender, EventArgs e)
        {
            OpenFileDialog open_paramfile_dialog = new OpenFileDialog();
            open_paramfile_dialog.Filter = "Text Files (.txt)|*.txt|All Files (*.*)|*.*";
            open_paramfile_dialog.FilterIndex = 1;
            if (open_paramfile_dialog.ShowDialog() == DialogResult.OK)
            {
                parameterfile_filename = open_paramfile_dialog.FileName;
                selectionarray[2] = true;
            }
            Parameter_File_Textbox.Text = parameterfile_filename;
        }

        private void open_dest_folder(object sender, EventArgs e)
        {
            FolderBrowserDialog open_destn_folder = new FolderBrowserDialog();
            if (open_destn_folder.ShowDialog() == DialogResult.OK)
            {
                destinationfolder_filepath = open_destn_folder.SelectedPath;
                selectionarray[3] = true;
            }
            Destination_textbox.Text = destinationfolder_filepath;
        }

        private void save_to_parameter(object sender, EventArgs e)
        {
            entryarray[0] = Name_File_Textbox.Text;
            entryarray[1] = Presentation_File_Textbox.Text;
            entryarray[2] = Parameter_File_Textbox.Text;
            entryarray[3] = Destination_textbox.Text;
            entryarray[4] = Location_X_Textbox.Text;
            entryarray[5] = Location_Y_Textbox.Text;
            entryarray[6] = Certificate_Name_Textbox.Text;
            if (selectionarray[2] == false)
                MessageBox.Show("         No parameter file has been selected, \n     You need a text file to save parameters to", "Oops, there was an error");
            else
            {
                var savefile = File.ReadAllLines(Parameter_File_Textbox.Text);
                for (int i = 0; i < 7; i++)
                {
                    if (selectionarray[i] == true)
                    {
                        savefile[i] = null;
                        savefile[i] += entryarray[i];
                    }
                }
                File.WriteAllLines(Parameter_File_Textbox.Text, savefile);
            }
                

        }


        public string lastname;
        public string fullname;
        public string filename;
        public bool nametaken;

        private void create_certificates(object sender, EventArgs e)
        {
            //If save file radio button selected, check for parameter file
            //If use current settings radio button selected, check all settings are filled in
            if (Parameter_File_Textbox.Text == "")
            {
                MessageBox.Show("No parameter file selected");
            }
            else
            {
                var savefile = File.ReadAllLines(Parameter_File_Textbox.Text);
                string firstname;
                string middlename;

                int namecount = 2;
                Excel.Application namespreadsheet = new Excel.Application();
                if (namespreadsheet == null)
                    MessageBox.Show("Excel is not properly installed");
                if (savefile[1] == null)
                    MessageBox.Show("No presentation file in parameter file");
                Powerpoint.Application presentationfile = new Powerpoint.Application();
                var presentation = presentationfile.Presentations;
                var file = presentation.Open(savefile[1]);
                Powerpoint.Slide slide = file.Slides[1];
                float width = file.Slides.Application.ActivePresentation.PageSetup.SlideWidth;
                Powerpoint.Shape textbox = slide.Shapes.AddTextbox(Microsoft.Office.Core.MsoTextOrientation.msoTextOrientationHorizontal, Convert.ToInt32(savefile[4]), Convert.ToInt32(savefile[5]), width, 40);
                textbox.TextFrame.TextRange.Font.Name = savefile[7];
                textbox.TextFrame.TextRange.Font.Size = Convert.ToInt32(savefile[8]);
                if (savefile[9] == "Bold")
                    textbox.TextFrame.TextRange.Font.Bold = Microsoft.Office.Core.MsoTriState.msoTrue;
                else
                    textbox.TextFrame.TextRange.Font.Bold = Microsoft.Office.Core.MsoTriState.msoFalse;


                Excel.Workbook namespreadsheetwrkbk = namespreadsheet.Workbooks.Open(savefile[0], 0, true);
                Excel.Worksheet namespreadsheetwrksht = (Excel.Worksheet)namespreadsheetwrkbk.Worksheets.get_Item(1);
                firstname = (namespreadsheetwrksht.Cells[1, 1] as Excel.Range).Value;
                middlename = (namespreadsheetwrksht.Cells[1, 2] as Excel.Range).Value;
                while (firstname != null)
                {
                    firstname = (namespreadsheetwrksht.Cells[namecount, 1] as Excel.Range).Value;
                    if (firstname == null)
                        break;
                    middlename = (namespreadsheetwrksht.Cells[namecount, 2] as Excel.Range).Value;
                    lastname = (namespreadsheetwrksht.Cells[namecount, 3] as Excel.Range).Value;
                    if (middlename == null)
                        fullname = firstname + " " + lastname;
                    else
                        fullname = firstname + " " + middlename + " " + lastname;

                    textbox.TextFrame.TextRange.Text = fullname;
                    textbox.TextFrame.TextRange.ParagraphFormat.Alignment = Powerpoint.PpParagraphAlignment.ppAlignCenter;

                    filename = savefile[3] + "\\" + savefile[6] + lastname + ".pdf";
                    if (File.Exists(filename) == true)
                        filename = savefile[3] + "\\" + savefile[6] + middlename + "_" + lastname + ".pdf";


                    file.ExportAsFixedFormat(filename, Powerpoint.PpFixedFormatType.ppFixedFormatTypePDF, Powerpoint.PpFixedFormatIntent.ppFixedFormatIntentScreen);


                    namecount++;
                }


                System.Runtime.InteropServices.Marshal.ReleaseComObject(slide);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(textbox);
                file.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(file);
                System.Runtime.InteropServices.Marshal.ReleaseComObject(presentation);
                presentationfile.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(presentationfile);

                System.Runtime.InteropServices.Marshal.ReleaseComObject(namespreadsheetwrksht);
                namespreadsheetwrkbk.Close();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(namespreadsheetwrkbk);
                namespreadsheet.Quit();
                System.Runtime.InteropServices.Marshal.ReleaseComObject(namespreadsheet);

                this.Close();

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();

            }
        }

        private void X_Text_Changed(object sender, EventArgs e)
        {
            selectionarray[4] = true;
        }

        private void Y_Text_Changed(object sender, EventArgs e)
        {
            selectionarray[5] = true;
        }

        private void Certificate_Name_Change(object sender, EventArgs e)
        {
            selectionarray[6] = true;
        }


    }
}
