**Awesome Certificate Wizard**

**Summary**\
This program produces batches of PDF certificates from a powerpoint template and excel name list.

**Why**\
Years ago I worked at a company which determined energy efficiency ratings for new home constructions. A coworker of mine would occasionally host educational workshops for professionals in the field.
These workshops counted for learning credits in a "continuing education" program and as such would come with certificates of completion for those who attended. The preferred method for creating these
certificates was giving the intern a list of names in an excel spreadsheet and having them add each name and create a pdf manually. Obviously I cannot in good conscience provide the actual names list
or certificate that were used, even if now they are outdated, so a dummy certficate and fake list of names are provided. You will have to take my word for it that with a name list 20-30 entries long 
an automated solution to this process saved a lot of time. It was also interesting to learn that Microsoft Office products all have useful programming backends, easily accessible with C#.\
The label of "awesome" was an inside joke based on other software solutions at the company, the label "wizard" is just a joke on Windows software.

**How**\
The program takes information from a few different files:\
Parameter File.txt, (.txt), saves all of the relevant data for producing the certificates\
Name File.xlsx, (.xls or .xlsx), a file with the list of names to append to the certificate\
Certificate.pptx, (.ppt or .pptx), the certificate template file with blank name space\

After selecting those you designate a few other parameters:\
Destination folder, the directory where you would like the finished certificates saved\
The x distance (from the left) and y distance (from the top) of where you'd like the name centered.\
The series name (e.g. "Certificate_DD_MM_YYYY_")

When all of your information has been entered, hit "Save current settings" to overwrite the parameter file with this new information.\
This saves the settings to the parameter file, any criteria left blank will not be overwritten.

In the parameter file you can change some other options like the font type and size, as well as embolden it.

When everything in the parameter file is as you'd like it, hit "Create Awesome Certificates" to begin the process.

The program will:
- Open power point
- Make a text box at the coordinates
- Read the next name from the list in the excel file and write it into the text box in the powerpoint
- Center the text in the textbox
- Export the certificate as a PDF with the name "certificate series name"+"the person's last name" to the destination folder

Note: It is not required that you set up the parameter file every time. If none of the parameters have been changed, e.g. the only changes were within the xls or ppt files, 
then you can go directly from selecting the parameter file to creating certificates.

**Checksum**\
MD5..........Awesome Certificate Wizard.exe.....37beda318346cd32ad48cdeb0e200fb1\
SHA256.....Awesome Certificate Wizard.exe.....5a1d2984156dd3506622b16bcef5eadaa1bfb7f5c8186e71fa7e824d2c241b12

**License**\
I have made this program available to you under the GPL V2 license.\
You are free to use the program as you choose but any distribution of the program/source or modifications made to it must abide by restrictions of the license.

**Note for programmers**\
Part of interfacing with Excel/Powerpoint required an x86 dependency "aspnet_regsql," so be sure to adjust your build configuration to target x86 rather than x64 or "Any CPU."